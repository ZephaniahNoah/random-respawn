package com.zephaniahnoah.randomrespawn;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.ImmutableList;

public class Main extends JavaPlugin implements Listener {

	private static File config;
	private static List<int[]> positions = new ArrayList<int[]>();
	private static Random rand = new Random();
	private static final ImmutableList<String> removespawn = ImmutableList.of("removerandomspawn", "removespawn", "removerandspawn", "rrs");
	private static final ImmutableList<String> setspawn = ImmutableList.of("setrandomspawn", "randspawn", "srs");
	private static boolean removeMode = false;
	private static int index = -1;

	@Override
	public void onEnable() {
		String path = "plugins" + File.separator + this.getName() + File.separator;
		config = new File(path + "spawns.txt");
		try {
			if (config.exists()) {
				Scanner s = new Scanner(config);
				while (s.hasNextLine()) {
					String[] line = s.nextLine().split(",");
					positions.add(new int[] { parse(line[0]), parse(line[1]), parse(line[2]) });
				}
				s.close();
			} else {
				File f = new File(path);
				if (!f.exists()) {
					f.mkdir();
				}
				config.createNewFile();
			}
		} catch (Exception e) {
			System.out.println("Failed to initiate config: " + e.getMessage());
			e.printStackTrace();
		}
		this.getServer().getPluginManager().registerEvents(this, this);
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		if (positions.size() > 0) {
			int r = rand.nextInt(positions.size());
			int[] pos = positions.get(r);
			Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				@Override
				public void run() {
					e.getPlayer().teleport( new Location(e.getRespawnLocation().getWorld(), pos[0] + .5, pos[1], pos[2] - .5));
				}
			});
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			Location loc = p.getLocation();
			if (removespawn.contains(label.toLowerCase()) && testOp(p)) {
				if (removeMode) {
					positions.remove(index);
					removeMode = false;
					index = -1;
					saveConfig();
					p.sendMessage("Removed..");
				} else {
					double distance = Double.MAX_VALUE;
					Location pos = null;
					for (int i = 0; i < positions.size(); i++) {
						pos = new Location(p.getWorld(), positions.get(i)[0], positions.get(i)[1], positions.get(i)[2]);
						double testDistance = pos.distance(loc);
						if (testDistance < distance) {
							distance = testDistance;
							index = i;
						}
					}
					if (pos == null) {
						p.sendMessage("There are no random spawnpoints.");
					} else {
						p.sendMessage("Would you like to remove the spawn at x: " + pos.getX() + " y: " + pos.getY() + " z: " + pos.getZ() + " ?");
						p.sendMessage("Run this command again to confirm.");
						removeMode = true;
						Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
							@Override
							public void run() {
								if (removeMode) {
									p.sendMessage("Took too long to remove spawnpoint. Ignoring command...");
									removeMode = false;
									index = -1;
								}
							}
						}, 200L);
					}
				}
			} else if (setspawn.contains(label.toLowerCase()) && testOp(p)) {
				positions.add(new int[] { (int) loc.getX(), (int) loc.getY(), (int) loc.getZ() });
				saveConfig();
				p.sendMessage("Added spawnpoint..");
			}
		} else {
			sender.sendMessage("Only players can use this command.");
		}
		return false;
	}

	private boolean testOp(Player p) {
		boolean o = p.isOp();
		if (!o) {
			p.sendMessage("You must be OP to use that command.");
		}
		return o;
	}

	@Override
	public void saveConfig() {
		try {
			FileWriter w = new FileWriter(config);
			for (int i = 0; i < positions.size(); i++) {
				int[] n = positions.get(i);
				w.write(n[0] + "," + n[1] + "," + n[2] + "\n");
			}
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static int parse(String s) {
		return Integer.parseInt(s);
	}
}
